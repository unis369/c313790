### required packages

The following packages have to be installed to run all the code examples. Note that the lines to install the packages only have to be run once.


################# PACKAGE       # SECTION IN ARTICLE
#install.packages("readtext")    # data preparation
#install.packages("stringi")     # data preparation
#install.packages("quanteda")    # data preparation and analysis
#install.packages("topicmodels") # analysis
#install.packages("spacyr")      # advanced topics
#install.packages("corpustools") # advanced topics
library(readtext)  
library(stringi) 
library(quanteda)


#Data Preparation
----------------

### String Operations


 
text <-  c(d1 = "Some Wonderful smartphone under 10000",  
           d2 = "Smartphone is very bad",  
           d3 = "Best Smartphone of the year,very good",
           d4 = "Smartphone is very good") 
dtm <- dfm(text,                           # input text
           tolower = TRUE, stem = TRUE,    # set lowercasing and stemming to TRUE
           remove = stopwords("english"))  # provide the stopwords for deletion
dtm

### Filtering and weighting
doc_freq <- docfreq(dtm)         # document frequency per term (column) 
dtm <- dtm[, doc_freq >= 1]      # select terms with doc_freq >= 2 
dtm <- dfm_tfidf(dtm,base=10)  # weight the features using tf-idf 
head(dtm)
gc()


dtm



